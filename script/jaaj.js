const TABLEAU_IMAGES = [
    {
        'lien': 'image/eureka1.png',
    },
    {
        'lien': 'image/eureka2.png',
    },
    {
        'lien': 'image/eureka3.png',
    },
    {
        'lien': 'image/naruto.png',
    }
];

let indexImage = 0;

function changerDirection(direction){
    let SRCimage    = document.getElementById('imageSlider');

    if(direction == -1) {
        if(indexImage -1 < 0) {
            indexImage = TABLEAU_IMAGES.length -1;
        } else{
            indexImage --;
        } 
    }else{
        if((indexImage + 1) >= TABLEAU_IMAGES.length){
            indexImage = 0;
        }else{
            indexImage ++;
        }
    }
    SRCimage.setAttribute('src', TABLEAU_IMAGES[indexImage].lien);


}

document.getElementById('BTNprecendent').addEventListener('click', function(){
    changerDirection(-1);
});

document.getElementById('BTNsuivant').addEventListener('click', function(){
    changerDirection(+1);
});




var flipflop= false;

function flipflop_menuStatut(e){
    if (flipflop == true) {
        flipflop = false;
        document.getElementsByClassName('sidenav')[0].style.display = 'none';
    } else {
        flipflop = true;
        document.getElementsByClassName('sidenav')[0].style.display = 'block';
    }
}

document.getElementsByClassName('BTNnav')[0].addEventListener('click',flipflop_menuStatut);

var flipflop2= false;

function flipflop_menuStatut2(e){
    if (flipflop2 == true) {
        flipflop2 = false;
        document.getElementsByClassName('sidenav2')[0].style.display = 'none';
    } else {
        flipflop2 = true;
        document.getElementsByClassName('sidenav2')[0].style.display = 'block';
    }
}

document.getElementsByClassName('BTNconnection')[0].addEventListener('click',flipflop_menuStatut2);